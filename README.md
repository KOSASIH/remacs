[![DeepSource](https://deepsource.io/gh/KOSASIH/REMACS.svg/?label=active+issues&show_trend=true&token=HXi0MaFwgvTuSDeP6_iEUPW1)](https://deepsource.io/gh/KOSASIH/REMACS/?ref=repository-badge)
[![Documentation Status](https://readthedocs.org/projects/remacs/badge/?version=latest)](https://remacs.readthedocs.io/en/latest/?badge=latest)
[![CircleCI](https://circleci.com/gh/KOSASIH/REMACS/tree/main.svg?style=svg)](https://circleci.com/gh/KOSASIH/REMACS/tree/main)
[![Netlify Status](https://api.netlify.com/api/v1/badges/4d2dbcc2-f709-43bd-b0dc-a7af9a29538e/deploy-status)](https://app.netlify.com/sites/remacs/deploys)    
# REMACS
Disaster Recovery database management system
